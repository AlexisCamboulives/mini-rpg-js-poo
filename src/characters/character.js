
export default class Character {
    level = 1;
    constructor(health, hitStrength) {
        this.health = health;
        this.hitStrength = hitStrength;
    }
    getHealth() {
        return this.health;
    }
    setHealth(damage) {
        this.health -= damage;
    }
    attack() {
        return this.hitStrength * this.level;
    }
}