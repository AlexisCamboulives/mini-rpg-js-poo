import Player from './characters/character.js';
import Ork from './characters/ork.js';
import Goblin from './characters/goblin.js';
import Battle from './battle.js';
import Character from './characters/character.js';

let player = new Player(10, 10);
let ork = new Ork(10, 5);
let goblin = new Goblin(5, 1);

let battle = new Battle(player, ork);

console.log("Le premier combat face à l'orc des cavernes commence, bonne chance aventurier!");

battle.fight(player, ork);


if (player.getHealth() > 0) {
    console.log("Vous allez maintenanant combattre contre le gobelin des montagnes")
    battle.fight(player, goblin);
}

if (player.getHealth() > 0) {
    console.log("Vous avez gagné tout les combats !! Félicitation ")
}
